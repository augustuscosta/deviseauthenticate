package br.com.otgmobile.deviseauthenticate;

import roboguice.activity.RoboActivity;
import roboguice.inject.InjectView;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import br.com.otgmobile.deviseauthenticate.cloud.RestClient;
import br.com.otgmobile.deviseauthenticate.cloud.SessionCloud;
import br.com.otgmobile.deviseauthenticate.util.AppHelper;
import br.com.otgmobile.deviseauthenticate.util.ConstUtil;
import br.com.otgmobile.deviseauthenticate.util.LogUtil;

public class LoginActivity extends RoboActivity {
    
    private UserLoginTask mAuthTask = null;

    // Values for email and password at the time of the login attempt.
    private String mEmail;
    private String mPassword;

    // UI references.
    @InjectView(R.id.email) private EditText mEmailView;
    @InjectView(R.id.password)  private EditText mPasswordView;
    @InjectView(R.id.login_form) private View mLoginFormView;
    @InjectView(R.id.login_status) private View mLoginStatusView;
    @InjectView(R.id.login_status_message) private TextView mLoginStatusMessageView;
    @InjectView(R.id.sign_in_button) private Button mSignInButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);
        LogUtil.initLog(this, "DEVISE AUTHENTICATION");
        mEmailView.setText(mEmail);

        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

    }
    
    public void attemptLogin(View view){
    	attemptLogin();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.activity_login, menu);
        return true;
    }

    public void attemptLogin() {
        if (mAuthTask != null) {
            return;
        }

        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        mEmail = mEmailView.getText().toString();
        mPassword = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password.
        if (TextUtils.isEmpty(mPassword)) {
            mPasswordView.setError(getString(R.string.error_field_required));
            focusView = mPasswordView;
            cancel = true;
        } else if (mPassword.length() < 4) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(mEmail)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!mEmail.contains("@")) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            mLoginStatusMessageView.setText(R.string.login_progress_signing_in);
            showProgress(true);
            mAuthTask = new UserLoginTask(this);
            mAuthTask.execute();
        }
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginStatusView.setVisibility(View.VISIBLE);
            mLoginStatusView.animate()
                    .setDuration(shortAnimTime)
                    .alpha(show ? 1 : 0)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            mLoginStatusView.setVisibility(show ? View.VISIBLE : View.GONE);
                        }
                    });

            mLoginFormView.setVisibility(View.VISIBLE);
            mLoginFormView.animate()
                    .setDuration(shortAnimTime)
                    .alpha(show ? 0 : 1)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                        }
                    });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mLoginStatusView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class UserLoginTask extends AsyncTask<String, Void, RestClient> {
    	Context context;
    	SessionCloud cloud;
    	String erroMessage = null;
    	
    	UserLoginTask(Context context) {
			this.context = context;
		}
    	
        @Override
        protected RestClient doInBackground(String... params) {
            return login();
        }
        
        RestClient login(){
			cloud = new SessionCloud(context);
			try {
				cloud.login(mEmailView.getText().toString(), mPasswordView.getText().toString());
			} catch (Throwable e) {
				erroMessage = e.getLocalizedMessage();
				return cloud;
			}
			if(cloud.getResponseCode() != 200){
				erroMessage = cloud.getErrorMessage();
				return cloud;
			}
			return cloud;
		}

        @Override
        protected void onPostExecute(final RestClient cloud) {
            mAuthTask = null;
            showProgress(false);
            
            if (erroMessage != null) {
				if(cloud.getResponseCode() == ConstUtil.INVALID_TOKEN){
					AppHelper.getInstance().presentError(context,"Erro na conexão: ","Response code: " + cloud.getResponseCode());
					mPasswordView.setError(getString(R.string.error_incorrect_password));
	                mPasswordView.requestFocus();
				}else{
					AppHelper.getInstance().presentError(context,"Erro na conexão: ",erroMessage);
				}
			} else {
				finish();
			}

        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showProgress(false);
        }

    }
}
