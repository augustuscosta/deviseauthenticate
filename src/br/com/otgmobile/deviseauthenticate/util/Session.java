package br.com.otgmobile.deviseauthenticate.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.telephony.TelephonyManager;

public class Session {
	

	public static final String PREFS 						 = "SESSION_PREFS";
	public static final String SERVER_PREFS_KEY 			 = "SERVER_PREFS_KEY";
	public static final String TOKEN_PREFS_KEY 				 = "REST_TOKEN_PREFS_KEY";

	private static SharedPreferences settings;
 
	private static SharedPreferences getSharedPreferencesInstance(Context context){
		if(settings == null){
			settings = context.getSharedPreferences(PREFS, Context.MODE_PRIVATE);
		}
		return settings;
	}
	
	public static String getDeviceId(Context context){
		TelephonyManager TelephonyMgr = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
		String szImei = TelephonyMgr.getDeviceId();
		return szImei;
	}
	
	public static void setServer(String server,Context context){
		settings = getSharedPreferencesInstance(context);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(SERVER_PREFS_KEY, server);
		editor.commit();
	}
	
	public static String getServer(Context context){				
		return getSharedPreferencesInstance(context).getString(SERVER_PREFS_KEY, "http://10.0.2.2:3001");
	}
	
	public static String getToken(Context context){
		String token = getSharedPreferencesInstance(context).getString(TOKEN_PREFS_KEY, null);
		if(token == null || token.length() < 1 ){
			return null;
		}
		return token;
	}
	
	public static void setToken(String token,Context context){
		settings = getSharedPreferencesInstance(context);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(TOKEN_PREFS_KEY, token);
		editor.commit();
	}
	
	public static void clearSession(Context context){
		Session.setToken("", context);
	}
	

}
